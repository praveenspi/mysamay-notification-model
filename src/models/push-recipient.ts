import { MetadataHelper } from "@neb-sports/mysamay-common-utils";




export class PushRecipient {
    _id: string = null;
    notificationId: string = null;
    userId: string = null;
    userName: string = null;
    timestamp: Date = null;

    constructor(data: Partial<PushRecipient>) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(key === "timestamp") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                } 
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Notifications_PushRecipients";
}