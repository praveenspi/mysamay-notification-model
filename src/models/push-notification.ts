import { MetadataHelper } from "@neb-sports/mysamay-common-utils";



const dateFields = ["timestamp"];

export class PushNotification {
    _id: string = null;
    message: string = null;
    title: string = null;
    sentBy: string = null;
    userCount: number = null;
    sentByName: string = null;
    timestamp: Date = null;

    constructor(data: Partial<PushNotification>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.timestamp) {
            this.timestamp = new Date();
        }
    }

    static collectionName = "Notifications_Push";
}